package com.dca.phast;

/**
 * Created by User on 10/12/2017.
 */

public class AppConstants {

    public static final int MY_PERMISSION_REQUEST_CALL = 1002;
    public static final int CODE_OPEN_QUIZ_ACTIVITY = 1001;
    public static final int MY_PERMISSION_REQUEST_FINE_LOCATION = 1003;

    public static final String CODE = "code";
    public static final String TITLE = "title";
    public static final String TITLE_NEP = "title_nep";
    public static final String TYPE = "type";
    public static final String SUB_CODE = "sub_code";
    public static final String TOOL_INTRO_TEXT = "tool_intro_text";
    public static final String TOOL_INTRO_TEXT_NEP = "tool_intro_text_nep";

    public static final String MSG_1 = "msg_1";
    public static String MSG_1_NEP = "msg_1_nep";
}
