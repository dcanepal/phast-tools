package com.dca.phast.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.dca.phast.AppConstants;

import static android.support.v4.app.ActivityCompat.requestPermissions;

/**
 * Created by User on 6/15/2017.
 */

public class PermissionUtils {

    public static void permissionGranted(Context context) {
        //   Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show();
    }

    public static void permissionDenied(Context context) {
        //  Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show();
    }

    public static boolean checkCallPermission(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestCallPermission(Activity activity) {
        requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, AppConstants.MY_PERMISSION_REQUEST_CALL);
    }

    public static boolean checkFineLocationPermission(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestFineLocationPermission(Activity activity) {
        requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.MY_PERMISSION_REQUEST_FINE_LOCATION);
    }

}
