package com.dca.phast.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;

import com.dca.phast.R;

/**
 * Created by Chamling on 6/25/2018.
 */

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        TextView objTv = findViewById(R.id.obj_tv);
        String objText = getString(R.string.text_obj_org);
        objTv.setText(Html.fromHtml(objText));
    }
}
