package com.dca.phast;

import android.app.Application;
import android.content.Context;

/**
 * Created by User on 1/17/2018.
 */

public class MyApp extends Application {

    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
}
