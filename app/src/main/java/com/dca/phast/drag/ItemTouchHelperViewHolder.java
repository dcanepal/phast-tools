package com.dca.phast.drag;

/**
 * Created by Chamling on 6/2/2018.
 */

public interface ItemTouchHelperViewHolder {

    void onItemSelected();

    void onItemClear();
}
