package com.dca.phast.drag;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dca.phast.AppConstants;
import com.dca.phast.R;
import com.dca.phast.data.Details;
import com.dca.phast.data.abstraction.DataAbstraction;
import com.dca.phast.details.fragments.BottomSheetFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by Chamling on 6/16/2018.
 */

public class DragImageActivityNew extends AppCompatActivity {

    private static final String TAG = DragImageActivityNew.class.getSimpleName();
    ItemTouchHelper mTouchHelper;
    private String code;
    private String subcode;
    private String type;
    private String title;
    private String titleNep;
    private String toolIntroText;
    private String toolIntroTextNep;

    RelativeLayout frameLayout;
    FrameLayout fl;

    private ImageView finalIv;
    private int itemCount = 0;
    TextView totalAttemptTv;

    CountDownTimer countDownTimer;
    SweetAlertDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_image_new);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        getBundleData();

        initViews();

        countDownTimer = new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                dialog.dismissWithAnimation();
                // notifierIv.setVisibility(View.GONE);
            }
        };

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
    }


    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(AppConstants.CODE))
                code = extras.getString(AppConstants.CODE);
            if (extras.containsKey(AppConstants.SUB_CODE))
                subcode = extras.getString(AppConstants.SUB_CODE);
            if (extras.containsKey(AppConstants.TYPE))
                type = extras.getString(AppConstants.TYPE);
            if (extras.containsKey(AppConstants.TITLE))
                title = extras.getString(AppConstants.TITLE);
            if (extras.containsKey(AppConstants.TITLE_NEP))
                titleNep = extras.getString(AppConstants.TITLE_NEP);
            if (extras.containsKey(AppConstants.TOOL_INTRO_TEXT))
                toolIntroText = extras.getString(AppConstants.TOOL_INTRO_TEXT);
            if (extras.containsKey(AppConstants.TOOL_INTRO_TEXT_NEP))
                toolIntroTextNep = extras.getString(AppConstants.TOOL_INTRO_TEXT_NEP);

            final Bundle bundle = new Bundle();
            bundle.putString(AppConstants.TOOL_INTRO_TEXT, toolIntroText);
            bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, toolIntroTextNep);
            bundle.putString(AppConstants.TITLE, title);
            bundle.putString(AppConstants.TITLE_NEP, titleNep);

            // showing btm sheet dialog with some lag
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showBtmSheetDialogFrag(bundle);
                }
            }, 200);
        }
    }

    public interface ItemMatchListener {
        void itemMatched(ImageView draggedView, int noOfAttempts);

        void itemNotMatched(int noOfAttempts);
    }

    public ItemMatchListener matchListener = new ItemMatchListener() {
        public final String TAG = ItemMatchListener.class.getSimpleName();

        @Override
        public void itemMatched(ImageView draggedView, int noOfAttempts) {

            fl.removeView(draggedView);
            ++itemCount;
            showSnackMsg("Correct");

            updateAttempt(noOfAttempts);
           /* for (int i = 0; i < myModelList.size(); i++) {
                if (myModelList.get(i).getPath().equalsIgnoreCase(tag)) {
                    Log.d(TAG, myModelList.get(i).getPath() + " : " + tag);
                    myModelList.remove(i);
                    break;
                }
            }
            mBookingAdapter.updateData(myModelList);*/
        }

        private void updateAttempt(int noOfAttempts) {
            totalAttemptTv.setText("कुल प्रयास : " + noOfAttempts);
            totalAttemptTv.setVisibility(View.VISIBLE);
        }

        @Override
        public void itemNotMatched(int noOfAttempts) {
            showSnackMsg("Wrong");
            updateAttempt(noOfAttempts);
        }
    };

    private void showSnackMsg(String message) {
        //  Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show();
        if (message.equalsIgnoreCase("Wrong")) {
            dialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("जोडी मिलेन ");
            // notifierIv.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_wrong));
        } else
            dialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("जोडी मिल्यो ");
        if (itemCount >= 4) {
            dialog.setContentText("रोकथामको पहिलो उपाय हेर्नुहोस |");
            // notifierIv.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.GONE);
        }
        countDownTimer.cancel();
        dialog.show();
        if (itemCount < 4) {
            dialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
            countDownTimer.start();
        } else {
            final Button okBtn = dialog.findViewById(R.id.confirm_button);
            okBtn.setText("हुन्छ ");
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismissWithAnimation();
                    setFinalImage();
                }
            });
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews() {
        AppCompatImageButton infoBtn = findViewById(R.id.info_btn);
        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.TITLE, title);
                bundle.putString(AppConstants.TITLE_NEP, titleNep);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT, toolIntroText);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, toolIntroTextNep);
                showBtmSheetDialogFrag(bundle);
            }
        });

        RecyclerView quizRv = findViewById(R.id.quiz_rv);
        quizRv.setHasFixedSize(true);
        quizRv.setLayoutManager(new LinearLayoutManager(this));

        List<Details> quizList = getDetails(this, code, subcode);
        List<Details> mShuffledQuizList = new ArrayList<>(quizList);
        //TODO randomize @Param quizList here
        Collections.shuffle(quizList);

        Resources r = getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, r.getDisplayMetrics());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = (height - px) / 4;

        SelectionAdapter adapter = new SelectionAdapter(this, quizList, params);
        quizRv.setAdapter(adapter);

        finalIv = findViewById(R.id.final_iv);

        ImageDragListener imageDragListener = new ImageDragListener(this, matchListener);

        frameLayout = findViewById(R.id.drag_fl);
        fl = findViewById(R.id.fl);

        ImageView ivOneHolder = findViewById(R.id.iv_one_holder);
        ivOneHolder.setOnDragListener(imageDragListener);
        ivOneHolder.setLayoutParams(params);
        ivOneHolder.setTag(quizList.get(0).getQuiz_text());
        Log.d(TAG, "img tag: " + quizList.get(0).getQuiz_text());

        ImageView ivTwoHolder = findViewById(R.id.iv_two_holder);
        ivTwoHolder.setOnDragListener(imageDragListener);
        ivTwoHolder.setLayoutParams(params);
        ivTwoHolder.setTag(quizList.get(1).getQuiz_text());
        Log.d(TAG, quizList.get(1).getQuiz_text());

        ImageView ivThreeHolder = findViewById(R.id.iv_three_holder);
        ivThreeHolder.setOnDragListener(imageDragListener);
        ivThreeHolder.setLayoutParams(params);
        ivThreeHolder.setTag(quizList.get(2).getQuiz_text());
        Log.d(TAG, quizList.get(2).getQuiz_text());

        ImageView ivFourHolder = findViewById(R.id.iv_four_holder);
        ivFourHolder.setOnDragListener(imageDragListener);
        ivFourHolder.setLayoutParams(params);
        ivFourHolder.setTag(quizList.get(3).getQuiz_text());
        Log.d(TAG, quizList.get(3).getQuiz_text());

        Collections.shuffle(mShuffledQuizList);
        ImageView ivOneDrag = findViewById(R.id.iv_one_drag);
        ImageView ivTwoDrag = findViewById(R.id.iv_two_drag);
        ImageView ivThreeDrag = findViewById(R.id.iv_three_drag);
        ImageView ivFourDrag = findViewById(R.id.iv_four_drag);

        for (int i = 0; i < quizList.size(); i++) {
            Details details = quizList.get(i);
            if (details.getQuiz_text().equalsIgnoreCase(mShuffledQuizList.get(0).getQuiz_text())) {
                ivOneDrag.setLayoutParams(getFrameLayoutParams(height, px, 8));
                Picasso.with(this)
                        .load(getResources().getIdentifier("@drawable/" + mShuffledQuizList.get(0).getQuiz_text(), null, getPackageName()))
                        .into(ivOneDrag);
                ivOneDrag.setTag(mShuffledQuizList.get(0).getQuiz_text());
            } else if (details.getQuiz_text().equalsIgnoreCase(mShuffledQuizList.get(1).getQuiz_text())) {
                ivTwoDrag.setLayoutParams(getFrameLayoutParams(height, px, 16));
                Picasso.with(this)
                        .load(getResources().getIdentifier("@drawable/" + mShuffledQuizList.get(1).getQuiz_text(), null, getPackageName()))
                        .into(ivTwoDrag);
                ivTwoDrag.setTag(mShuffledQuizList.get(1).getQuiz_text());
            } else if (details.getQuiz_text().equalsIgnoreCase(mShuffledQuizList.get(2).getQuiz_text())) {
                ivThreeDrag.setLayoutParams(getFrameLayoutParams(height, px, 24));
                Picasso.with(this)
                        .load(getResources().getIdentifier("@drawable/" + mShuffledQuizList.get(2).getQuiz_text(), null, getPackageName()))
                        .into(ivThreeDrag);
                ivThreeDrag.setTag(mShuffledQuizList.get(2).getQuiz_text());
            } else {
                ivFourDrag.setLayoutParams(getFrameLayoutParams(height, px, 32));
                Picasso.with(this)
                        .load(getResources().getIdentifier("@drawable/" + mShuffledQuizList.get(3).getQuiz_text(), null, getPackageName()))
                        .into(ivFourDrag);
                ivFourDrag.setTag(mShuffledQuizList.get(3).getQuiz_text());
            }
        }

        totalAttemptTv = findViewById(R.id.total_attempt_tv);
        totalAttemptTv.setVisibility(View.GONE);

        /*
        ImageView ivOneDrag = findViewById(R.id.iv_one_drag);
        ivOneDrag.setLayoutParams(getFrameLayoutParams(height, px, 8));
        Picasso.with(this)
                .load(getResources().getIdentifier("@drawable/" + quizList.get(0).getQuiz_text(), null, getPackageName()))
                .into(ivOneDrag);
        ivOneDrag.setTag(quizList.get(0).getQuiz_text());

        ImageView ivTwoDrag = findViewById(R.id.iv_two_drag);
        ivTwoDrag.setLayoutParams(getFrameLayoutParams(height, px, 16));
        Picasso.with(this)
                .load(getResources().getIdentifier("@drawable/" + quizList.get(1).getQuiz_text(), null, getPackageName()))
                .into(ivTwoDrag);
        ivTwoDrag.setTag(quizList.get(1).getQuiz_text());

        ImageView ivThreeDrag = findViewById(R.id.iv_three_drag);
        ivThreeDrag.setLayoutParams(getFrameLayoutParams(height, px, 24));
        Picasso.with(this)
                .load(getResources().getIdentifier("@drawable/" + quizList.get(2).getQuiz_text(), null, getPackageName()))
                .into(ivThreeDrag);
        ivThreeDrag.setTag(quizList.get(2).getQuiz_text());

        ImageView ivFourDrag = findViewById(R.id.iv_four_drag);
        ivFourDrag.setLayoutParams(getFrameLayoutParams(height, px, 32));
        Picasso.with(this)
                .load(getResources().getIdentifier("@drawable/" + quizList.get(3).getQuiz_text(), null, getPackageName()))
                .into(ivFourDrag);
        ivFourDrag.setTag(quizList.get(3).getQuiz_text());
*/
        ivOneDrag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //      ClipData data = ClipData.newPlainText("", (CharSequence)holder.imageView.getTag());
                // Create a new ClipData.Item from the ImageView object's tag
                ClipData.Item item = new ClipData.Item((String) v.getTag());

                // Create a new ClipData using the tag as a label, the plain text MIME type, and
                // the already-created item. This will create a new ClipDescription object within the
                // ClipData, and set its MIME type entry to "text/plain"
                ClipData dragData = new ClipData((CharSequence) v.getTag(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);


                //   View.DragShadowBuilder myShadow = new MyDragShadowBuilder(holder.imageView);
                View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
                v.startDrag(dragData,
                        myShadow,
                        v,
                        0);
                return false;
            }
        });

        ivTwoDrag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //      ClipData data = ClipData.newPlainText("", (CharSequence)holder.imageView.getTag());
                // Create a new ClipData.Item from the ImageView object's tag
                ClipData.Item item = new ClipData.Item((String) v.getTag());

                // Create a new ClipData using the tag as a label, the plain text MIME type, and
                // the already-created item. This will create a new ClipDescription object within the
                // ClipData, and set its MIME type entry to "text/plain"
                ClipData dragData = new ClipData((CharSequence) v.getTag(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);


                //   View.DragShadowBuilder myShadow = new MyDragShadowBuilder(holder.imageView);
                View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
                v.startDrag(dragData,
                        myShadow,
                        v,
                        0);
                return false;
            }
        });


        ivThreeDrag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //      ClipData data = ClipData.newPlainText("", (CharSequence)holder.imageView.getTag());
                // Create a new ClipData.Item from the ImageView object's tag
                ClipData.Item item = new ClipData.Item((String) v.getTag());

                // Create a new ClipData using the tag as a label, the plain text MIME type, and
                // the already-created item. This will create a new ClipDescription object within the
                // ClipData, and set its MIME type entry to "text/plain"
                ClipData dragData = new ClipData((CharSequence) v.getTag(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);


                //   View.DragShadowBuilder myShadow = new MyDragShadowBuilder(holder.imageView);
                View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
                v.startDrag(dragData,
                        myShadow,
                        v,
                        0);
                return false;
            }
        });

        ivFourDrag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //      ClipData data = ClipData.newPlainText("", (CharSequence)holder.imageView.getTag());
                // Create a new ClipData.Item from the ImageView object's tag
                ClipData.Item item = new ClipData.Item((String) v.getTag());

                // Create a new ClipData using the tag as a label, the plain text MIME type, and
                // the already-created item. This will create a new ClipDescription object within the
                // ClipData, and set its MIME type entry to "text/plain"
                ClipData dragData = new ClipData((CharSequence) v.getTag(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);

                //   View.DragShadowBuilder myShadow = new MyDragShadowBuilder(holder.imageView);
                View.DragShadowBuilder myShadow = new View.DragShadowBuilder(v);
                v.startDrag(dragData,
                        myShadow,
                        v,
                        0);
                return false;
            }
        });

        ImageView notifierIv = findViewById(R.id.notifier_iv);
        notifierIv.setVisibility(View.GONE);
    }

    private ViewGroup.LayoutParams getFrameLayoutParams(int height, int px, int margin) {
        FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.END | Gravity.RIGHT);
        frameParams.height = (int) ((height - px) / 3.5);
        frameParams.width = (height - px) / 3;
        frameParams.setMargins(0, margin, margin, 0);
        return frameParams;
    }

    private void setFinalImage() {
        String path = "@drawable/ic_dmr_50";
        try {
            int res = getResources().getIdentifier(path, null, getPackageName());
            Picasso.with(this).load(res).into(finalIv);
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage());
        }

    }

    private List<Details> getDetails(Context context, String code, String subcode) {
        List<Details> detailsList = new ArrayList<>();
        try {
            detailsList = DataAbstraction.getInstance(context).getDetailsByCode(code, subcode);
            Log.d(TAG, "list size:::" + detailsList.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return detailsList;
    }

    private static class SelectionAdapter extends RecyclerView.Adapter<ItemViewHolder> {

        private static final String TAG = SelectionAdapter.class.getSimpleName();

        private List<Details> mDetailsList;
        private Context mContext;
        LinearLayout.LayoutParams mParams;

        public SelectionAdapter(Context context, List<Details> detailsList, LinearLayout.LayoutParams params) {
            mContext = context;
            mDetailsList = detailsList;
            mParams = params;
        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_selection, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ItemViewHolder holder, int position) {

            holder.itemView.setLayoutParams(mParams);

            Details details = mDetailsList.get(position);
            Log.d(TAG, details.getQuiz_text() + " on Position " + position);

            String uri = "@drawable/" + details.getImg();
            int imageResource;
            imageResource = mContext.getResources().getIdentifier(uri,
                    null,
                    mContext.getPackageName());
            Picasso.with(mContext).load(imageResource).into(holder.imageView);
        }

        @Override
        public int getItemCount() {
            return mDetailsList.size();
        }

    }

    private static class ImageDragListener implements View.OnDragListener {

        private static final String TAG = "ImageDragListener";

        private Context mContext;
        private ItemMatchListener mItemMatchListener;
        private int attemptCounter;


        public ImageDragListener(Context context, ItemMatchListener matchListener) {
            mContext = context;
            mItemMatchListener = matchListener;
            attemptCounter = 0;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {
            ImageView containerView = (ImageView) v;
            final ImageView draggedView = (ImageView) event.getLocalState();
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    Log.d(TAG, "onDrag: ACTION_DRAG_STARTED");

                    //    containerView.setColorFilter(Color.YELLOW);
                    // Invalidate the view to force a redraw in the new tint
                    v.invalidate();

                    return true;
                case DragEvent.ACTION_DRAG_ENTERED:
                    Log.d(TAG, "onDrag: ACTION_DRAG_ENTERED");

                    // Invalidate the view to force a redraw in the new tint
                    v.invalidate();
                    return true;
                case DragEvent.ACTION_DRAG_EXITED:
                    Log.d(TAG, "onDrag: ACTION_DRAG_EXITED");
                    return true;
                case DragEvent.ACTION_DROP:
                    // Gets the item containing the dragged data
                    ClipData.Item item = event.getClipData().getItemAt(0);
                    // Gets the text data from the item.
                    CharSequence dragData = item.getText();

                    Log.d(TAG, "holder: " + containerView.getTag() + " --> drag: " + dragData);

                    if (String.valueOf(dragData).equalsIgnoreCase(String.valueOf(containerView.getTag()))) {
                        String path = "@drawable/" + dragData;

                        try {

                            int res = mContext.getResources().getIdentifier(path, null, mContext.getPackageName());
                            Picasso.with(mContext).load(res).into(containerView);
                        } catch (Exception e) {
                            Log.d(TAG, e.getLocalizedMessage());
                        }
                        containerView.setOnDragListener(null);
                        mItemMatchListener.itemMatched(draggedView, ++attemptCounter);
                    } else {
                        mItemMatchListener.itemNotMatched(++attemptCounter);
                    }
                    return true;
                case DragEvent.ACTION_DRAG_ENDED:
                    Log.d(TAG, "onDrag: ACTION_DRAG_ENDED");
                    //   containerView.setColorFilter(Color.WHITE);
                    return true;
                default:
                    return true;
            }
        }
    }

    private void showBtmSheetDialogFrag(Bundle bundle) {
        BottomSheetFragment bottomSheetFragment = BottomSheetFragment.newInstance();
        if (bundle != null)
            bottomSheetFragment.setArguments(bundle);
        try {
            bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

}
