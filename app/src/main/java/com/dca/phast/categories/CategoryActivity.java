package com.dca.phast.categories;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dca.phast.AppConstants;
import com.dca.phast.R;
import com.dca.phast.about.AboutUsActivity;
import com.dca.phast.data.Categories;
import com.dca.phast.data.abstraction.DataAbstraction;
import com.dca.phast.details.DetailActivityNew;
import com.dca.phast.drag.DragImageActivityNew;
import com.dca.phast.utils.ItemOffsetDecoration;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 10/10/2017.
 */

public class CategoryActivity extends AppCompatActivity implements CategoryContract.View {

    private static final String TAG = CategoryActivity.class.getSimpleName();
    private static final int GRID_SPAN_COUNT = 2;
    private CategoryContract.Presenter mPresenter;
    private CategoriesAdapter categoriesAdapter;
    private String code;
    private String subcode;
    private String type;
    private String title;
    private String tool_intro_text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        initViews();

        new CategoryPresenter(CategoryActivity.this);

        try {
            DataAbstraction dataAbstraction = DataAbstraction.getInstance(this);
            mPresenter.load(dataAbstraction);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    private CategoryItemListener mListener = new CategoryItemListener() {

        @Override
        public void onCategoryClick(String subcode,
                                    String type,
                                    String title,
                                    String titleNep,
                                    String text,
                                    String textNep) {
            Bundle bundle = new Bundle();
            if (type.equalsIgnoreCase("drag")) {
                Intent intent = new Intent(CategoryActivity.this, DragImageActivityNew.class);
                bundle.putString(AppConstants.CODE, code);
                bundle.putString(AppConstants.TITLE, title);
                bundle.putString(AppConstants.TITLE_NEP, titleNep);
                bundle.putString(AppConstants.TYPE, type);
                bundle.putString(AppConstants.SUB_CODE, subcode);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT, text);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, textNep);
                intent.putExtras(bundle);
                startActivity(intent);
            } else {
                Intent intent = new Intent(CategoryActivity.this, DetailActivityNew.class);
                bundle.putString(AppConstants.CODE, code);
                bundle.putString(AppConstants.TITLE, title);
                bundle.putString(AppConstants.TITLE_NEP, titleNep);
                bundle.putString(AppConstants.TYPE, type);
                bundle.putString(AppConstants.SUB_CODE, subcode);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT, text);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, textNep);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }

    };

    private void initViews() {

        categoriesAdapter = new CategoriesAdapter(this, new ArrayList<Categories>(0), mListener);
        final RecyclerView categoriesRv = findViewById(R.id.category_rv);
        categoriesRv.setHasFixedSize(true);
        categoriesRv.setNestedScrollingEnabled(false);
        categoriesRv.setFocusable(false);
        categoriesRv.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.item_offset));

        categoriesRv.setLayoutManager(new GridLayoutManager(this, 2));
        categoriesRv.setAdapter(categoriesAdapter);

        ImageButton aboutUsBtn = findViewById(R.id.about_us);
        aboutUsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CategoryActivity.this, AboutUsActivity.class));
            }
        });
    }

    @Override
    public void setPresenter(CategoryContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showCategories(List<Categories> categoriesList) {
        categoriesAdapter.replaceData(categoriesList);
    }

    @Override
    public void showNoCategories() {
    }

    public static class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

        private List<Categories> categoryList;
        private CategoryItemListener mListener;
        private Context mContext;

        CategoriesAdapter(Context context, List<Categories> categoryList, CategoryItemListener listener) {
            mContext = context;
            mListener = listener;
            setData(categoryList);
        }

        private void setData(List<Categories> categoryList) {
            this.categoryList = categoryList;
            notifyDataSetChanged();
        }

        private void replaceData(List<Categories> categoriesList) {
            setData(categoriesList);
        }

        @NonNull
        @Override
        public CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category, parent, false);
            return new CategoriesViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final CategoriesViewHolder holder, final int position) {
            final Categories category = categoryList.get(position);

            String code = category.getCode();


            setTextBg(code, holder.titleTv);

            setTextHolderBg(code, holder.footerRv);

            setCardBg(code, holder.imageView);


            holder.titleTv.setText(category.getTitle());
            holder.titleTv.setVisibility(View.GONE);

            String uri = "@drawable/" + categoryList.get(position).getImage();
            try {
                int imageResource = mContext.getResources().getIdentifier(uri, null, mContext.getPackageName());
                try {
                    Picasso.with(mContext)
                            .load(imageResource)
                            .into(holder.imageView);
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onCategoryClick(
                            category.getCode(),
                            category.getType(),
                            category.getTitle(),
                            category.getTitleNep(),
                            category.getText(),
                            category.getTextNep()
                    );
                }
            });
        }

        private void setCardBg(String code, ImageView holder) {
            if (code.equalsIgnoreCase("SHP"))
                holder.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.white));
            else
                holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorSecondary));
        }

        private void setTextHolderBg(String code, RelativeLayout holder) {

            if (code.equalsIgnoreCase("SHP"))
                holder.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.white));
            else
                holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorSecondary));

        }

        private void setTextBg(String code, TextView holder) {

            if (code.equalsIgnoreCase("MBZ"))
                holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.maroon));
            else
                holder.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue));
        }

        @Override
        public int getItemCount() {
            return categoryList.size();
        }

        static class CategoriesViewHolder extends RecyclerView.ViewHolder {

            TextView titleTv;
            ImageView imageView;
            RelativeLayout footerRv;

            CategoriesViewHolder(View itemView) {
                super(itemView);
                titleTv = itemView.findViewById(R.id.title_tv);
                imageView = itemView.findViewById(R.id.content_iv);
                footerRv = itemView.findViewById(R.id.footer_rv);


            }
        }
    }

    public interface CategoryItemListener {
        void onCategoryClick(String code, String categoryType,
                             String subcode, String titleNep, String text, String textNep);
    }

}
