package com.dca.phast.categories;

import com.dca.phast.data.Categories;
import com.dca.phast.data.abstraction.DataAbstraction;

import java.util.List;

/**
 * Created by User on 10/10/2017.
 */

public class CategoryPresenter implements CategoryContract.Presenter {

    private final CategoryContract.View mView;

    CategoryPresenter(CategoryContract.View mView) {
        this.mView = mView;
        mView.setPresenter(this);
    }

    @Override
    public void load(DataAbstraction dataAbstraction) {
        // List<Categories> categoriesList = dataAbstraction.getCategoriesByCode(code);
        List<Categories> categoriesList = dataAbstraction.getCategories();
        processCategories(categoriesList);
    }

    private void processCategories(List<Categories> categoriesList) {
        if (categoriesList.size() > 0)
            mView.showCategories(categoriesList);
        else
            mView.showNoCategories();
    }
}
