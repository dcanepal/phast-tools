package com.dca.phast.categories;

import com.dca.phast.BasePresenter;
import com.dca.phast.BaseView;
import com.dca.phast.data.Categories;
import com.dca.phast.data.abstraction.DataAbstraction;

import java.util.List;

/**
 * Created by User on 10/10/2017.
 */

public interface CategoryContract {

    interface View extends BaseView<Presenter> {

        void showCategories(List<Categories> categoriesList);

        void showNoCategories();
    }

    interface Presenter extends BasePresenter {

        void load(DataAbstraction dataAbstraction);
    }
}
