package com.dca.phast.quiz;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.dca.phast.R;
import com.dca.phast.data.Details;
import com.dca.phast.quiz.frag.InconclusiveSelFragment;
import com.dca.phast.quiz.frag.RightSelFragment;
import com.dca.phast.quiz.frag.WrongSelFragment;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 10/24/2017.
 */

public class QuizActivity extends AppCompatActivity implements QuizContract.View {

    private String code, subcode, title;

    private static final String TAG = QuizActivity.class.getSimpleName();
    private QuizContract.Presenter mPresenter;
    private List<Details> correctSelList;
    private List<Details> indecisiveSelList;
    private List<Details> wrongSelList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_new);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        getBundleData();

        initViews();

        new QuizPresenter(this);

        //      mPresenter.load(this, code, subcode, 0);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
    }


    private void initViews() {

        ViewPager vpPager = findViewById(R.id.viewpager);
        MyPagerAdapter adapterViewPager = new MyPagerAdapter(this, getSupportFragmentManager(), correctSelList, indecisiveSelList, wrongSelList);
        vpPager.setAdapter(adapterViewPager);

        SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(vpPager);
    }

    private void getBundleData() {

        correctSelList = new ArrayList<>();
        indecisiveSelList = new ArrayList<>();
        wrongSelList = new ArrayList<>();

        List<Details> mDetailsList = getIntent().getParcelableArrayListExtra("mList");

        mDetailsList.remove(mDetailsList.size() - 1);

        for (Details d : mDetailsList) {
            Log.d(TAG, d.getImg() + " : " + d.getAns());
            if (d.getAns() == 0)
                correctSelList.add(d);
            else if (d.getAns() == 1)
                indecisiveSelList.add(d);
            else
                wrongSelList.add(d);
        }
    }


    @Override
    public void setPresenter(QuizContract.Presenter presenter) {
        mPresenter = presenter;
    }
    @Override
    public void showNoData() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 3;
        private List<Details> mCorrectSelList, mIndecisiveSelList, mWrongSelList;
        private Context mContext;

        public MyPagerAdapter(Context context, FragmentManager fragmentManager, List<Details> correctSelList, List<Details> indecisiveSelList, List<Details> wrongSelList) {
            super(fragmentManager);
            mContext = context;
            mCorrectSelList = correctSelList;
            mIndecisiveSelList = indecisiveSelList;
            mWrongSelList = wrongSelList;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return RightSelFragment.newInstance(mCorrectSelList);
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return InconclusiveSelFragment.newInstance(mIndecisiveSelList);
                case 2: // Fragment # 1 - This will show SecondFragment
                    return WrongSelFragment.newInstance(mWrongSelList);
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return mContext.getString(R.string.title_good_habit);
                case 1:
                    return mContext.getString(R.string.title_inbetween_habit);
                case 2:
                    return mContext.getString(R.string.title_bad_habit);
                default:
                    break;
            }
            return "";
        }

    }

}
