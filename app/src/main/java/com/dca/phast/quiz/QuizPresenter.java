package com.dca.phast.quiz;

import android.content.Context;

/**
 * Created by User on 11/16/2017.
 */

public class QuizPresenter implements QuizContract.Presenter {

    private final QuizContract.View mView;

    QuizPresenter(QuizContract.View mView) {
        this.mView = mView;
        mView.setPresenter(this);
    }

    @Override
    public void load(Context context, String code, String subcode, int level) {
        getQuizQuestions(context, code, subcode, level);
    }

    private void getQuizQuestions(Context context, String code, String subcode, int level) {
    }

}
