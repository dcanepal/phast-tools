package com.dca.phast.quiz.frag;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dca.phast.R;
import com.dca.phast.data.Details;
import com.dca.phast.utils.ItemOffsetDecoration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Chamling on 6/18/2018.
 */

public class RightSelFragment extends Fragment {
    // Store instance variables
    private List<Details> mCorrectSelList;
    private final static String TAG = RightSelFragment.class.getSimpleName();

    // newInstance constructor for creating fragment with arguments
    public static RightSelFragment newInstance(List<Details> correctSelList) {
        RightSelFragment fragmentFirst = new RightSelFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("mList", (ArrayList<? extends Parcelable>) correctSelList);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCorrectSelList = getArguments().getParcelableArrayList("mList");
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_right_sel, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "list size: " + mCorrectSelList.size());

        TextView emptyTv = view.findViewById(R.id.empty_tv);
        if (mCorrectSelList.size() > 0)
            emptyTv.setVisibility(View.GONE);

        RightSelAdapter adapter = new RightSelAdapter(getActivity(), mCorrectSelList);
        RecyclerView selelctionRv = view.findViewById(R.id.selection_rv);
        selelctionRv.addItemDecoration(new ItemOffsetDecoration(getActivity(), R.dimen.item_offset_big));
        selelctionRv.setHasFixedSize(true);
        selelctionRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        selelctionRv.setAdapter(adapter);
    }


    public static class RightSelAdapter extends RecyclerView.Adapter<RightSelAdapter.ViewHolder> {

        private List<Details> detailList;
        private Context mContext;

        RightSelAdapter(Context context, List<Details> detailList) {
            mContext = context;
            setData(detailList);
        }

        private void setData(List<Details> detailList) {
            this.detailList = detailList;
            notifyDataSetChanged();
        }

        private void replaceData(List<Details> categoriesList) {
            setData(categoriesList);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_selection, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            String uri = "@drawable/" + detailList.get(position).getImg();
            Log.d(TAG, "uri: " + uri);
            try {
                int imageResource = mContext.getResources().getIdentifier(uri, null, mContext.getPackageName());
                Log.d(TAG, "imageResource: " + imageResource);
                try {
                    Picasso.with(mContext)
                            .load(imageResource)
                            .into(holder.selectionImage);
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return detailList.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {

            ImageView selectionImage;

            ViewHolder(View itemView) {
                super(itemView);
                selectionImage = itemView.findViewById(R.id.selection_iv);
            }
        }
    }

}