package com.dca.phast.quiz;

import android.content.Context;

import com.dca.phast.BasePresenter;
import com.dca.phast.BaseView;

/**
 * Created by User on 11/16/2017.
 */

public class QuizContract {

    interface View extends BaseView<Presenter> {

        void showNoData();
    }

    interface Presenter extends BasePresenter {
        void load(Context context, String code, String subcode, int level);
    }
}
