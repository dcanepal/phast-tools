package com.dca.phast.details;

import android.content.Context;
import android.content.Intent;

import com.dca.phast.BasePresenter;
import com.dca.phast.BaseView;
import com.dca.phast.data.Details;

import java.util.List;

/**
 * Created by User on 10/11/2017.
 */

public interface DetailsContract {

    interface View extends BaseView<Presenter> {

        void showCards(List<Details> detailsList);

        void showNoCards();

        void fireIntent(Intent browserIntent);
    }

    interface Presenter extends BasePresenter {

        void load(Context context, String code, String subcode);

        void prepareBrowserIntent(String url);

        void preparePhoneDialIntent(String phoneNo);
    }
}
