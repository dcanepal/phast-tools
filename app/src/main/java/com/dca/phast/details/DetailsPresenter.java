package com.dca.phast.details;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.dca.phast.data.Details;
import com.dca.phast.data.abstraction.DataAbstraction;

import org.json.JSONException;

import java.util.List;

/**
 * Created by User on 10/11/2017.
 */

public class DetailsPresenter implements DetailsContract.Presenter {

    private DetailsContract.View mView;
    private Context context;
    private static final String TAG = DetailsPresenter.class.getSimpleName();

    public DetailsPresenter(DetailsContract.View mView) {
        this.mView = mView;
        mView.setPresenter(this);
    }

    @Override
    public void load(Context context, String code, String subcode) {
        loadDetails(context, code, subcode);
    }

    private void loadDetails(Context context, String code, String subcode) {
        getDetails(context, code, subcode);
    }

    private void getDetails(Context context, String code, String subcode) {
        try {
            List<Details> detailsList = DataAbstraction.getInstance(context).getDetailsByCode(code, subcode);
            Log.d(TAG, "list size:::" + detailsList.size());
            processDetails(detailsList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processDetails(List<Details> testData) {
        if (testData.size() > 0)
            mView.showCards(testData);
        else
            mView.showNoCards();
    }

    @Override
    public void preparePhoneDialIntent(String phoneNo) {
        Intent dialIntent = new Intent(Intent.ACTION_DIAL);
        dialIntent.setData(Uri.parse("tel:" + phoneNo));
        mView.fireIntent(dialIntent);
    }

    @Override
    public void prepareBrowserIntent(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mView.fireIntent(browserIntent);
    }
}
