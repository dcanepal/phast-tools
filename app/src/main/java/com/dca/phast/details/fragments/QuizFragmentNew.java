package com.dca.phast.details.fragments;

/**
 * Created by Chamling on 7/17/2018.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.StackView;
import android.widget.TextView;

import com.dca.phast.AppConstants;
import com.dca.phast.R;
import com.dca.phast.data.Details;
import com.dca.phast.details.DetailsContract;
import com.dca.phast.details.DetailsPresenter;
import com.dca.phast.quiz.QuizActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by User on 11/16/2017.
 */

public class QuizFragmentNew extends Fragment implements DetailsContract.View, View.OnClickListener {

    private static final int TYPE_RIGHT = 0;
    private static final int TYPE_INCONCLUSIVE = 1;
    private static final int TYPE_WRONG = 2;
    private DetailsContract.Presenter mPresenter;
    private String code, subcode, title, titleNep, toolIntroText, toolIntroTextNep;

    private static final String TAG = QuizFragmentNew.class.getSimpleName();

    private View mView;

    private StackView stackView;

    private int listPosCount, counter;
    private List<Details> mDetailsList;
    private ProgressBar pBar;
    private int listSize;
    private ImageView iv;
    private int[] imgResArr = new int[50];
    private TextView counterTv;

    private Button rightBtn, inconclusiveBtn, wrongBtn;

    public static QuizFragmentNew newInstance() {
        return new QuizFragmentNew();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        code = getArguments().getString(AppConstants.CODE);
        subcode = getArguments().getString(AppConstants.SUB_CODE);
        title = getArguments().getString(AppConstants.TITLE);
        titleNep = getArguments().getString(AppConstants.TITLE_NEP);
        toolIntroText = getArguments().getString(AppConstants.TOOL_INTRO_TEXT);
        toolIntroTextNep = getArguments().getString(AppConstants.TOOL_INTRO_TEXT_NEP);
        return inflater.inflate(R.layout.frag_quiz_new, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mView = view;

        new DetailsPresenter(QuizFragmentNew.this);

        mPresenter.load(getActivity(), code, subcode);

        listPosCount = 0;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews(List<Details> detailsList) {

        this.mDetailsList = detailsList;

        handleToolbar();

        counter = 0;

        pBar = mView.findViewById(R.id.progressbar);

        counterTv = mView.findViewById(R.id.counter_tv);
        counterTv.setVisibility(View.GONE);

        listSize = detailsList.size();

        ImageButton cancelBtn = mView.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(this);

        ImageButton infoBtn = mView.findViewById(R.id.info_btn);
        infoBtn.setOnClickListener(this);

        iv = mView.findViewById(R.id.iv);

        rightBtn = mView.findViewById(R.id.right_btn);
        rightBtn.setOnClickListener(this);
        inconclusiveBtn = mView.findViewById(R.id.inconclusive_btn);
        inconclusiveBtn.setOnClickListener(this);
        wrongBtn = mView.findViewById(R.id.wrong_btn);
        wrongBtn.setOnClickListener(this);

        displayNext();

        //    setUpProgress();
    }


    private void setUpProgress() {
        pBar.setVisibility(View.GONE);
        pBar.setProgress(((listPosCount + 1) * 100) / listSize);

        String counterText = getString(R.string.text_counter, String.valueOf(++counter), String.valueOf(listSize - 1));
        counterTv.setVisibility(View.VISIBLE);
        counterTv.setText(counterText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.right_btn:
                // setUpProgress();
                rightBtn.setEnabled(false);
                showNextCard(TYPE_RIGHT);
                break;
            case R.id.inconclusive_btn:
                inconclusiveBtn.setEnabled(false);
                // setUpProgress();
                showNextCard(TYPE_INCONCLUSIVE);
                break;
            case R.id.wrong_btn:
                wrongBtn.setEnabled(false);
                //  setUpProgress();
                showNextCard(TYPE_WRONG);
                break;
            case R.id.cancel_btn:
                getActivity().finish();
                break;
            case R.id.info_btn:
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.TITLE, title);
                bundle.putString(AppConstants.TITLE_NEP, titleNep);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT, toolIntroText);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, toolIntroTextNep);
                showBtmSheetDialogFrag(bundle);

                break;
        }
    }

    private void showBtmSheetDialogFrag(Bundle bundle) {
        BottomSheetFragment bottomSheetFragment = BottomSheetFragment.newInstance();
        if (bundle != null)
            bottomSheetFragment.setArguments(bundle);
        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
    }

    private void showNextCard(int type) {
        Log.d(TAG, "listPosCount: " + listPosCount + "  size: " + mDetailsList.size());
        if (listPosCount == mDetailsList.size() - 2) {
            mDetailsList.get(listPosCount).setAns(type);
            launchSelectionScreen();
        } else {
            mDetailsList.get(listPosCount).setAns(type);
            listPosCount = listPosCount + 1;
            displayNext();
            // stackView.showNext();
        }

    }

    private void displayNext() {
        startAnim();
        /*
        String uri = "@drawable/" + mDetailsList.get(listPosCount).getImg();
        try {
            int imageResource = getResources().getIdentifier(uri, null, getActivity().getPackageName());
            try {
                Picasso.with(getActivity())
                        .load(imageResource)
                        .into(iv);
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

    }

    private void launchSelectionScreen() {
        Intent intent = new Intent(getActivity(), QuizActivity.class);
        intent.putParcelableArrayListExtra("mList", (ArrayList<? extends Parcelable>) mDetailsList);
        startActivityForResult(intent, AppConstants.CODE_OPEN_QUIZ_ACTIVITY);
    }

    private void startAnim() {

        Animation anim = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out);
        //   anim.setInterpolator(new AccelerateInterpolator());
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                iv.setVisibility(View.GONE);
                Animation anim = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
                //         anim.setInterpolator(new DecelerateInterpolator());
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        iv.setVisibility(View.VISIBLE);
                        Picasso.with(getActivity())
                                .load(imgResArr[listPosCount])
                                .into(iv);
                        setUpProgress();
                        rightBtn.setEnabled(true);
                        inconclusiveBtn.setEnabled(true);
                        wrongBtn.setEnabled(true);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                iv.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        iv.startAnimation(anim);

    }

    private static class StackAdapter extends BaseAdapter {

        private List<Details> mDetailsList;
        private Context mContext;
        private LayoutInflater inflater;
        private ViewHolder viewHolder = null;

        public StackAdapter(Context context, List<Details> detailsList) {
            mContext = context;
            mDetailsList = detailsList;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mDetailsList.size();
        }

        @Override
        public Object getItem(int position) {
            return mDetailsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.row_stack, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.imageView = convertView.findViewById(R.id.stack_iv);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            String uri = "@drawable/" + mDetailsList.get(position).getImg();
            try {
                int imageResource = mContext.getResources().getIdentifier(uri, null, mContext.getPackageName());
                try {
                    Picasso.with(mContext)
                            .load(imageResource)
                            .into(viewHolder.imageView);
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return convertView;
        }

        public class ViewHolder {
            ImageView imageView;
        }
    }

    private void handleToolbar() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null)
            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void setPresenter(DetailsContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void showCards(List<Details> detailsList) {

        for (int i = 0; i < detailsList.size(); i++) {
            String uri = "@drawable/" + detailsList.get(i).getImg();
            try {
                int imageResource = getResources().getIdentifier(uri, null, getActivity().getPackageName());
                imgResArr[i] = imageResource;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        initViews(detailsList);
    }

    @Override
    public void showNoCards() {
    }

    @Override
    public void fireIntent(Intent browserIntent) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstants.CODE_OPEN_QUIZ_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                getActivity().onBackPressed();
            }
        }
    }
}
