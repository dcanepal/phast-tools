package com.dca.phast.details;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.dca.phast.AppConstants;
import com.dca.phast.R;
import com.dca.phast.details.fragments.BottomSheetFragment;
import com.dca.phast.details.fragments.CardFragment;
import com.dca.phast.details.fragments.QuizFragmentNew;
import com.dca.phast.utils.ActivityUtils;

/**
 * Created by User on 10/10/2017.
 */

public class DetailActivityNew extends AppCompatActivity {
    private String code;
    private String subcode;
    private String type;
    private String title;
    private String titleNep;
    private String toolIntroText;
    private String toolIntroTextNep;
    private static final String TAG = DetailActivityNew.class.getSimpleName();

    private void showBtmSheetDialogFrag(Bundle bundle) {
        BottomSheetFragment bottomSheetFragment = BottomSheetFragment.newInstance();
        if (bundle != null)
            bottomSheetFragment.setArguments(bundle);
        try {
            bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_new);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        getBundleData();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
    }

    private void getBundleData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(AppConstants.CODE)) {
                code = extras.getString(AppConstants.CODE);
            }
            if (extras.containsKey(AppConstants.SUB_CODE)) {
                subcode = extras.getString(AppConstants.SUB_CODE);
               /* if (subcode != null) {
                    if (subcode.equalsIgnoreCase("SHP") || subcode.equalsIgnoreCase("MSB")) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    } else {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    }
                }*/
            }
            if (extras.containsKey(AppConstants.TYPE))
                type = extras.getString(AppConstants.TYPE);
            if (extras.containsKey(AppConstants.TITLE))
                title = extras.getString(AppConstants.TITLE);
            if (extras.containsKey(AppConstants.TITLE_NEP))
                titleNep = extras.getString(AppConstants.TITLE_NEP);
            if (extras.containsKey(AppConstants.TOOL_INTRO_TEXT))
                toolIntroText = extras.getString(AppConstants.TOOL_INTRO_TEXT);
            if (extras.containsKey(AppConstants.TOOL_INTRO_TEXT_NEP))
                toolIntroTextNep = extras.getString(AppConstants.TOOL_INTRO_TEXT_NEP);

            final Bundle bundle = new Bundle();
            bundle.putString(AppConstants.TOOL_INTRO_TEXT, toolIntroText);
            bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, toolIntroTextNep);
            bundle.putString(AppConstants.TITLE, title);
            bundle.putString(AppConstants.TITLE_NEP, titleNep);

            // showing btm sheet dialog with some lag
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showBtmSheetDialogFrag(bundle);
                }
            }, 200);
        }
        getFragment(type);
    }

    private void getFragment(String type) {
        switch (type) {
            case "card":
                CardFragment cardFragment = CardFragment.newInstance();
                cardFragment.setArguments(getData());
                ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), cardFragment, R.id.fragment_container);
                DetailsPresenter detailsPresenter = new DetailsPresenter(cardFragment);
                break;
            case "selection":
                //   QuizFragment quizFragment = QuizFragment.newInstance();
                QuizFragmentNew quizFragment = QuizFragmentNew.newInstance();

                quizFragment.setArguments(getData());
                ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), quizFragment, R.id.fragment_container);
                detailsPresenter = new DetailsPresenter(quizFragment);
            default:
                break;
        }
    }

    private Bundle getData() {
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.CODE, code);
        bundle.putString(AppConstants.SUB_CODE, subcode);
        bundle.putString(AppConstants.TITLE, title);
        bundle.putString(AppConstants.TITLE_NEP, titleNep);
        bundle.putString(AppConstants.TOOL_INTRO_TEXT, toolIntroText);
        bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, toolIntroTextNep);
        return bundle;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return true;
    }
}
