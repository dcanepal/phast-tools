package com.dca.phast.details.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.daprlabs.cardstack.SwipeDeck;
import com.daprlabs.cardstack.SwipeFrameLayout;
import com.dca.phast.AppConstants;
import com.dca.phast.R;
import com.dca.phast.data.Details;
import com.dca.phast.details.DetailsContract;
import com.dca.phast.details.DetailsPresenter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 11/16/2017.
 */

public class CardFragment extends Fragment implements DetailsContract.View,
        View.OnClickListener {

    private final static String TAG = CardFragment.class.getSimpleName();

    private DetailsContract.Presenter mPresenter;
    private SwipeFrameLayout swipeFrameLayout;
    private SwipeDeck cardStack;
    private int cardPos = 0;
    private String code, subcode, title, titleNep, toolIntroText, toolIntroTextNep;
    private int listSize;
    private View view;
    private ProgressBar pBar;
    private List<Details> paginatedDetailsList, mDetailsList;
    private int paginationIndex = 4;
    private static final int START_PAGINATION_FROM = 2;
    private static final int NO_OF_PAGINATED_ITEMS = 8;

    public CardFragment() {
    }

    public static CardFragment newInstance() {
        return new CardFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        code = getArguments().getString(AppConstants.CODE);
        subcode = getArguments().getString(AppConstants.SUB_CODE);
        title = getArguments().getString(AppConstants.TITLE);
        titleNep = getArguments().getString(AppConstants.TITLE_NEP);
        toolIntroText = getArguments().getString(AppConstants.TOOL_INTRO_TEXT);
        toolIntroTextNep = getArguments().getString(AppConstants.TOOL_INTRO_TEXT_NEP);
        Log.d(TAG, "text: " + toolIntroText);

        return inflater.inflate(R.layout.frag_card, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;

        handleToolbar();

        paginatedDetailsList = new ArrayList<>();

        mDetailsList = new ArrayList<>();

        new DetailsPresenter(CardFragment.this);

        mPresenter.load(getActivity(), code, subcode);

        setUpProgress();

    }

    private void handleToolbar() {
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null)
            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    private void initViews(final List<Details> detailsList) {

        pBar = view.findViewById(R.id.progressbar);

        AppCompatImageView leftArrow = view.findViewById(R.id.left_arrow_iv);
        leftArrow.setOnClickListener(this);
        AppCompatImageView rightArrow = view.findViewById(R.id.right_arrow_iv);
        rightArrow.setOnClickListener(this);

        ImageButton cancelBtn = view.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(this);

        ImageButton infoBtn = view.findViewById(R.id.info_btn);
        infoBtn.setOnClickListener(this);

        cardStack = view.findViewById(R.id.swipe_deck1);
        cardStack.setHardwareAccelerationEnabled(true);

        swipeFrameLayout = view.findViewById(R.id.swipe_fl_1);

        SwipeDeckAdapter adapter = new SwipeDeckAdapter(detailsList, getActivity());

        cardStack.setAdapter(adapter);
        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                taskOnNextStory(position);
            }

            @Override
            public void cardSwipedRight(int position) {
                taskOnNextStory(position);
            }

            @Override
            public void cardsDepleted() {
                swipeFrameLayout.setVisibility(View.GONE);
                getActivity().finish();
            }

            @Override
            public void cardActionDown() {
            }

            @Override
            public void cardActionUp() {
            }
        });

    }

    private void taskOnNextStory(int position) {
        loadMoreStories(position);
        updateStoryAndProgress(position);
        Log.d(TAG, "card position: " + position);
        handleIfAdditionalText(position);
    }

    private void handleIfAdditionalText(int position) {
        int newPos = position + 1;
        try {
            Details details = mDetailsList.get(newPos);
            Log.d(TAG, details.toString());
            if (!details.getMsg1().isEmpty()) {
                final Bundle bundle = new Bundle();
                bundle.putString(AppConstants.MSG_1, details.getMsg1());
                bundle.putString(AppConstants.MSG_1_NEP, details.getMsg1Nep());
                showBtmSheetDialogFrag(bundle);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private void updateStoryAndProgress(int position) {
        cardPos = position + 1;
        setUpProgress();
    }

    private void loadMoreStories(final int position) {
        if (position == paginationIndex - START_PAGINATION_FROM) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = paginationIndex; j < paginationIndex + NO_OF_PAGINATED_ITEMS; j++) {
                        if (j < listSize) {
                            paginatedDetailsList.add(mDetailsList.get(j));
                        } else {
                            break;
                        }
                    }
                    paginationIndex = paginationIndex + NO_OF_PAGINATED_ITEMS;
                }
            }).start();
        }
    }

    private void setUpProgress() {
        pBar.setVisibility(View.VISIBLE);
        pBar.setProgress(((cardPos + 1) * 100) / listSize);
    }

    @Override
    public void setPresenter(DetailsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showCards(List<Details> detailsList) {
        listSize = detailsList.size();
        mDetailsList = detailsList;
        for (paginationIndex = 0; paginationIndex < 2; paginationIndex++)
            paginatedDetailsList.add(detailsList.get(paginationIndex));
        initViews(paginatedDetailsList);
    }

    @Override
    public void showNoCards() {
    }

    @Override
    public void fireIntent(Intent browserIntent) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_btn:
                swipeFrameLayout.setVisibility(View.GONE);
                getActivity().finish();
                break;
            case R.id.info_btn:
              /*  Intent intent = new Intent(getActivity(), TaskActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);*/
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.TITLE, title);
                bundle.putString(AppConstants.TITLE_NEP, titleNep);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT, toolIntroText);
                bundle.putString(AppConstants.TOOL_INTRO_TEXT_NEP, toolIntroTextNep);
                showBtmSheetDialogFrag(bundle);

                break;
            case R.id.left_arrow_iv:
                addLeftCard();
                break;
            case R.id.right_arrow_iv:
                addRightCard();
                break;
        }
    }

    private void addLeftCard() {
        if (cardPos != 0) {
            cardPos--;
            handleIfAdditionalText(cardPos - 1);
            cardStack.setSelection(cardPos);
            setUpProgress();
        } /*else
            onBackPressed();*/
    }

    private void addRightCard() {
        loadMoreStories(cardPos);

        if (cardPos != listSize - 1) {
            handleIfAdditionalText(cardPos);
            cardPos++;
            cardStack.setSelection(cardPos);
            setUpProgress();
        } else {
            swipeFrameLayout.setVisibility(View.GONE);
            getActivity().finish();
        }
        Log.d(TAG, String.valueOf(cardPos));
    }

    private static class SwipeDeckAdapter extends BaseAdapter {

        private final String TAG = SwipeDeckAdapter.class.getSimpleName();

        private List<Details> data;
        private Context context;
        private ImageView imageView;
        View v;

        public SwipeDeckAdapter(List<Details> data, Context context) {
            this.context = context;
            setData(data);
        }

        private void setData(List<Details> data) {
            this.data = data;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            v = convertView;
            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.card_detail_new, parent, false);
            }

            try {
                imageView = v.findViewById(R.id.image_iv);
                Details details = data.get(position);
                String uri = "@drawable/" + details.getImg();
                int imageResource;
                imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
                Picasso.with(context).load(imageResource).into(imageView);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            return v;
        }
    }

    private void showBtmSheetDialogFrag(Bundle bundle) {
        BottomSheetFragment bottomSheetFragment = BottomSheetFragment.newInstance();
        if (bundle != null)
            bottomSheetFragment.setArguments(bundle);

        bottomSheetFragment.show(getActivity().getSupportFragmentManager(), bottomSheetFragment.getTag());
    }
}
