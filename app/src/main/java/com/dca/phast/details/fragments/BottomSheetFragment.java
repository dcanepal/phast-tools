package com.dca.phast.details.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dca.phast.AppConstants;
import com.dca.phast.R;

/**
 * Created by Chamling on 6/12/2018.
 */

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private Bundle bundle;

    private final static String TAG = BottomSheetFragment.class.getSimpleName();

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    public static BottomSheetFragment newInstance() {
        return new BottomSheetFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                View bottomSheetInternal = d.findViewById(android.support.design.R.id.design_bottom_sheet);
                if (bottomSheetInternal != null) {
                    BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_sheet_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView titleTv = view.findViewById(R.id.title_tv);
        TextView titleTvNep = view.findViewById(R.id.title_tv_nep);
        TextView infoTv = view.findViewById(R.id.info_tv);
        TextView infoTvNep = view.findViewById(R.id.info_tv_nep);
        if (bundle.containsKey(AppConstants.TITLE)) {
            titleTv.setVisibility(View.VISIBLE);
            titleTv.setText(bundle.getString(AppConstants.TITLE));
        }

        if (bundle.containsKey(AppConstants.TOOL_INTRO_TEXT)) {
            infoTv.setVisibility(View.VISIBLE);
            infoTv.setTextSize(16f);
            infoTv.setGravity(Gravity.LEFT);
            infoTv.setText(bundle.getString(AppConstants.TOOL_INTRO_TEXT));

          /*  infoNepTv.setVisibility(View.VISIBLE);
            infoNepTv.setTextSize(16f);
            infoNepTv.setGravity(Gravity.LEFT);
            infoNepTv.setText(bundle.getString(AppConstants.TOOL_INTRO_TEXT));*/
        } else if (bundle.containsKey(AppConstants.MSG_1)) {
            Log.d(TAG, bundle.getString(AppConstants.MSG_1));
            infoTv.setVisibility(View.VISIBLE);
            infoTv.setTextSize(24f);
            infoTv.setGravity(Gravity.CENTER);
            infoTv.setText(bundle.getString(AppConstants.MSG_1));

        }

        if (bundle.containsKey(AppConstants.TITLE_NEP)) {
            titleTvNep.setVisibility(View.VISIBLE);
            titleTvNep.setText(bundle.getString(AppConstants.TITLE_NEP));
        }
        if (bundle.containsKey(AppConstants.TOOL_INTRO_TEXT_NEP)) {
            infoTvNep.setVisibility(View.VISIBLE);
            infoTvNep.setTextSize(16f);
            infoTvNep.setGravity(Gravity.LEFT);
            infoTvNep.setText((bundle.getString(AppConstants.TOOL_INTRO_TEXT_NEP)));

        } else if (bundle.containsKey(AppConstants.MSG_1_NEP)) {
            infoTvNep.setVisibility(View.VISIBLE);
            infoTvNep.setTextSize(24f);
            infoTvNep.setGravity(Gravity.CENTER);
            infoTvNep.setText(bundle.getString(AppConstants.MSG_1_NEP));
        }
    }
}