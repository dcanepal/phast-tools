package com.dca.phast.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

/**
 * Created by User on 10/11/2017.
 */

public class Details implements Parcelable {
    private String img;
    private String msg1;
    private String msg1Nep;
    private String text;
    private String quiz_text;
    private String quiz_nar;
    private String[] options;
    private String[] options_nar;
    private int ans;
    private boolean skip;
    private String correct_msg;
    private String correct_msg_nar;
    private String type;

    public Details() {
    }

    protected Details(Parcel in) {
        img = in.readString();
        msg1 = in.readString();
        msg1Nep = in.readString();
        text = in.readString();
        quiz_text = in.readString();
        quiz_nar = in.readString();
        options = in.createStringArray();
        options_nar = in.createStringArray();
        ans = in.readInt();
        skip = in.readByte() != 0;
        correct_msg = in.readString();
        correct_msg_nar = in.readString();
        type = in.readString();
    }

    public static final Creator<Details> CREATOR = new Creator<Details>() {
        @Override
        public Details createFromParcel(Parcel in) {
            return new Details(in);
        }

        @Override
        public Details[] newArray(int size) {
            return new Details[size];
        }
    };

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getQuiz_text() {
        return quiz_text;
    }

    public void setQuiz_text(String quiz_text) {
        this.quiz_text = quiz_text;
    }

    public String getQuiz_nar() {
        return quiz_nar;
    }

    public void setQuiz_nar(String quiz_nar) {
        this.quiz_nar = quiz_nar;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public String[] getOptions_nar() {
        return options_nar;
    }

    public void setOptions_nar(String[] options_nar) {
        this.options_nar = options_nar;
    }

    public int getAns() {
        return ans;
    }

    public void setAns(int ans) {
        this.ans = ans;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String getCorrect_msg() {
        return correct_msg;
    }

    public void setCorrect_msg(String correct_msg) {
        this.correct_msg = correct_msg;
    }

    public String getCorrect_msg_nar() {
        return correct_msg_nar;
    }

    public void setCorrect_msg_nar(String correct_msg_nar) {
        this.correct_msg_nar = correct_msg_nar;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsg1() {
        return msg1;
    }

    public void setMsg1(String msg1) {
        this.msg1 = msg1;
    }

    public String getMsg1Nep() {
        return msg1Nep;
    }

    public void setMsg1Nep(String msg1Nep) {
        this.msg1Nep = msg1Nep;
    }

    @Override
    public String toString() {
        return "Details{" +
                "img='" + img + '\'' +
                ", msg1='" + msg1 + '\'' +
                ", text='" + text + '\'' +
                ", quiz_text='" + quiz_text + '\'' +
                ", quiz_nar='" + quiz_nar + '\'' +
                ", options=" + Arrays.toString(options) +
                ", options_nar=" + Arrays.toString(options_nar) +
                ", ans=" + ans +
                ", skip=" + skip +
                ", correct_msg='" + correct_msg + '\'' +
                ", correct_msg_nar='" + correct_msg_nar + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(img);
        dest.writeString(msg1);
        dest.writeString(msg1Nep);
        dest.writeString(text);
        dest.writeString(quiz_text);
        dest.writeString(quiz_nar);
        dest.writeStringArray(options);
        dest.writeStringArray(options_nar);
        dest.writeInt(ans);
        dest.writeByte((byte) (skip ? 1 : 0));
        dest.writeString(correct_msg);
        dest.writeString(correct_msg_nar);
        dest.writeString(type);
    }
}
