package com.dca.phast.data.abstraction;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.dca.phast.data.Categories;
import com.dca.phast.data.Details;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by User on 10/11/2017.
 */

public class DataAbstraction {

    private static DataAbstraction INSTANCE = null;
    private static Context ctx;
    private static JSONObject taskObj;
    private static List<Details> detailsList = null;
    private final String TAG = DataAbstraction.class.getSimpleName();

    private static List<Categories> categories = null;

    public DataAbstraction(Context context) throws JSONException {
        ctx = context;
    }

    private JSONObject loadData() throws JSONException {
        if (taskObj == null) {
            try {
                taskObj = new JSONObject(loadJSONFromAsset(ctx));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return taskObj;
    }

    public static synchronized DataAbstraction getInstance(Context context) throws JSONException {
        if (INSTANCE == null) {
            INSTANCE = new DataAbstraction(context);
        }
        return INSTANCE;
    }

    public List<Categories> getCategories() {
        try {
            JSONArray categoriesArr = loadData().getJSONArray("data");
            Gson gson = new Gson();
            Type listType = new TypeToken<List<Categories>>() {
            }.getType();
            categories = gson.fromJson(categoriesArr.toString(), listType);
            return categories;
        } catch (JSONException e) {
            e.printStackTrace();
            return categories;
        }
    }

    public List<Details> getDetailsByCode(String code, String subcode) {
        if (categories.isEmpty() || categories == null)
            getCategories();
        for (Categories c : categories) {
            if (c.getCode().equalsIgnoreCase(subcode)) {
                detailsList = c.getDetails();
                return detailsList;
            }
        }
        return detailsList;
    }

    private static String loadJSONFromAsset(Context context) {
        String json;
        try {
            InputStream is = context.getAssets().open("com_dca_phast_data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
