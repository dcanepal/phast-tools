package com.dca.phast.data;

import java.util.ArrayList;

/**
 * Created by User on 10/10/2017.
 */

public class Categories {

    private String title;
    private String titleNep;
    private String image;
    private String text;
    private String textNep;
    private String code;
    private String type;
    private String video;
    private ArrayList<Details> details;

    public Categories() {
    }

    public Categories(String title, String titleNep, String image, String text, String textNep, String code, String type, String video, ArrayList<Details> details) {
        this.title = title;
        this.titleNep = titleNep;
        this.image = image;
        this.text = text;
        this.textNep = textNep;
        this.code = code;
        this.type = type;
        this.video = video;
        this.details = details;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleNep() {
        return titleNep;
    }

    public void setTitleNep(String titleNep) {
        this.titleNep = titleNep;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTextNep() {
        return textNep;
    }

    public void setTextNep(String textNep) {
        this.textNep = textNep;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public ArrayList<Details> getDetails() {
        return details;
    }

    public void setDetails(ArrayList<Details> details) {
        this.details = details;
    }
}
