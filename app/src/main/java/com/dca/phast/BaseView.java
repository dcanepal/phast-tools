package com.dca.phast;

/**
 * Created by User on 10/10/2017.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
